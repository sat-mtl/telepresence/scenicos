

# ScenicOS

[![pipeline status](https://gitlab.com/sat-mtl/distribution/scenicos/badges/develop/pipeline.svg)](https://gitlab.com/sat-mtl/distribution/scenicos/-/commits/develop)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)

## Overview

ScenicOS is a tool to create a bootable Ubuntu image used to run Scenic software suite headless.

ScenicOS boots a minimal X server and allow to run Scenic application deployed from Docker images.

ScenicOS uses Mender to allow remote management and over-the-air updates. Final image have 2 rootfs partitions allowing for safe OS updates. Mender-client service registers to Mender server, allowing monitoring, updates deployment and remote administration.

A data partition where Docker stores its containers, which is not erased when doing OS updates. This partition is initially 128 MB and automatically grows to fill all available space on device at first boot.

ScenicOS is currently maintained by the [Société des Arts Technologiques (SAT)](http://sat.qc.ca/), a non-profit artistic entity based in Montreal, Canada.

## Quick Start

Follow [ScenicOS Quick Start guide](docs/scenicos-quick-start.md) to learn how to bootstrap a device and launch Scenic remotely.

## Install to hard drive

Follow [ScenicOS Install guide](docs/scenicos-install.md) to learn how to install to device hard drive.

## Web Panel usage

ScenicOS comes with a webtop enabling a second X server in the browser. It allows to run utilities such as screen settings, X-Air-Edit mixer, OBS Studio, and QJackCtl. Follow [ScenicOS Web Panel guide](docs/scenicos-webpanel.md) to learn more.

## Build ScenicOS image

Follow [ScenicOS build instructions](docs/scenicos-build.md) to build disk image and Mender OS update artifact.

## Useful debug commands

A list of commands to remotely debug services is available [here](docs/scenicos-debug-commands.md).

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Acknowledgments

Thanks to Steve Leadbeater from Mender Hub forum for his simple [debootstrap example](https://hub.mender.io/t/efi-system-partitions/4008/7).

## License

This project is licensed under the GNU General Public License version 3 - see the [LICENSE](LICENSE.md) file for details.
