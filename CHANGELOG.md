Release Notes
===================

scenicos 1.2.0 (2023-07-14)
---------------------------------

 * 🚑 Migrate Mender to self-hosted instance ([!24](https://gitlab.com/sat-mtl/distribution/scenicos/-/merge_requests/24))
 * 🐛 Fix scenic-report logs path for Scenic 4.1 ([!23](https://gitlab.com/sat-mtl/distribution/scenicos/-/merge_requests/23))
 * 🧱 Improve Scenic services lifecycle management ([!21](https://gitlab.com/sat-mtl/distribution/scenicos/-/merge_requests/21))
 * 🐛 Fix scenic-report on large coredump ([!20](https://gitlab.com/sat-mtl/distribution/scenicos/-/merge_requests/20))
 * ✨ Simplify GPU configuration and add support for multiple NVIDIA cards ([!19](https://gitlab.com/sat-mtl/distribution/scenicos/-/merge_requests/19))

ScenicOS 1.1.1 (2023-03-28)
----------------------------------

* 🚑 Disable Mender artifact upload ([!16](https://gitlab.com/sat-mtl/distribution/scenicos/-/merge_requests/16))

ScenicOS 1.1.0 (2023-03-28)
----------------------------------

* Add GUI screenshot, text editor and zip archive tools
* Add nvidia-settings GUI tool
* Add keyboard layout switch applet and make ca layout default
* Add --disable-gpu option to scenic-open launcher for multi-gpu support
* Fix double bootloader issue
* Fix nvidia-smi return code 14 (infoROM is corrupted)
* Allow to overwrite deploy files
* Select Docker Compose file using Scenic-core tag
* Fetch station config script from Scenic project
* Remove useless Scenic config reset
* Remove Switcher global config from scenic-setup
* Cleanup Switcher global config when resetting config
* Remove configure-scenic-station.sh script
* Add Scenic logs viewer
* Add JACK logs viewer
* Add alsamixer launcher
* Add KXStudio Cadenc-jackmeter audio meter application
* Add KXStudio Catia JACK patch bay application
* Add X-AIR-Edit
* Add Chromium 107.0.5304.110 installed from chromium-browser-snapshots raw builds
* Add JACK configuration wizard
* Add Scenic reporter
* Add Scenic restart, stop, open launchers
* Add Scenic setup wizard
* Add REAPER setup wizard
* Add NoMachine setup wizard
* Add TeamViewer setup wizard
* Add Netdata setup wizard
* Add installer wizard
* Fix rootfs read-only race condition in mender-grow-data service
* Data partition auto-grow does not require a reboot anymore
* Allow to boot from any type of block device (vda,sda,nvme)
* Use mender-convert grub.d integration
* Add scenicos-image-builder Docker image for CI builds
* Use mender-convert directly from script, removes Docker dependency
* Add QEMU image test script
* Use lxterminal instead of xterm
* Add minimal lxpanel taskbar
* Use NetworkManager instead of netplan
* Configure CPU governor to performance at boot
* Add rtirq configuration for threaded irqs on snd and usb
* Fix home directory bind mount on data partition
* Fix drop to GRUB prompt on certain computers (removed unconfigured double UEFI bootloader)
* Configure mender-connect remote terminal to allow 10 concurrent sessions
* Configure mender-connect remote terminal to run as nobody user
* Add hdspmixer tool for RME sound interfaces
* Add lshw, lspci and lsusb debug tools
* Resize boot to 512M rootfs to 8192M, and data to 8192M
* Update mender-convert from 2.6.2 (2022-01-01) to 3.0.1 (2022-09-22)
* Update mender-client from 3.1.0 (2021-09-23) to 3.4.0 (2022-09-21)
* Update Nvidia driver to 510.108.03
* Update Linux to 5.13.0-52-lowlatency

ScenicOS 1.0.0 (2022-08-12)
----------------------------------

* 🔧 Patch nvenc to allow more than 3 sessions
* ✨ Add Magewell Eco Capture driver
* 💚 Build ScenicOS image from CI pipeline
* 📝 Fix typo in docker-compose URL in quick start
* 🔧 Update scenic configuration scripts to enable jack service
* ✨ Add Scenic Web Panel and documentation
* 📝 Add ScenicOS quick start guide
* 📝 Add GStreamer registry cache flush example
* 📝 Add more debug command examples
* 🔨 Add alsa-base to load snd_seq and fix missing /dev/snd/seq
* 🔧 Configure Jack systemd user service
* 🚚 Copy /home on /data/home
* 🚚 Bind mount /home on /data/home
* ✨ Add /data/storage directory for user files
* 🛂 Fix xhost permission
* ✨ Add avahi-daemon service
* 🔨 Add Docker Compose
* 🔧 Configure xhost +local to allow GUI Docker apps
* 📦 Add screen package
* ✨ Add Nvidia Container Toolkit to enable OpenGL in docker
* ✨ Fix ProCapture and Nvidia drivers installation
* ⚡ Increase rootfs from 4096 to 6192 MB
* 📦 Add artifact upload how-to using mender-cli
* 🔧 Use project hosted mender-convert Docker image
* 🛂 Update AUTHORS
* 🎉 Initial commit
