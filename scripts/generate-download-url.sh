#!/bin/bash

WEB_URL="http://scenicos-release.s3-website.ca-central-1.amazonaws.com/"
DIR_PATH=$1
FILE_PATH=$(find $DIR_PATH/*.img*)
FILENAME=$(basename $FILE_PATH)
echo "$WEB_URL$FILENAME"
echo "$WEB_URL$FILENAME.sha256sum"