#!/bin/bash

function print_usage() {
  cat << EOF
convert-base-image-to-mender.sh

Convert a base image to Mender format.

Usage: $0 BOARD_NAME

EOF
}

# Parse board name path argument
BOARD_NAME="${1}"

if [ -z "${BOARD_NAME}" ]; then
  echo -e "You need to pass BOARD_NAME path as argument.\n"
  print_usage
  exit 1
fi

BOARD_CONFIG_FILE=boards/$BOARD_NAME/board_config
if [ ! -e ${BOARD_CONFIG_FILE} ]; then
  echo -e "Cannot open file: ${BOARD_CONFIG_FILE}\n"
  exit 1
fi

echo "Running $(basename $0): $@"

# Load board configuration
source $BOARD_CONFIG_FILE

set -e

# Build version string from CI env. variables
# Final image name format i.e. scenicos-focal-amd64-x86-64-mender-develop-build23-dd648b2e-2022-07-28-1509.img.gz
BUILD_VERSION=""
if [ -n "$CI_PIPELINE_IID" ]; then
  if [ -n "$CI_COMMIT_TAG" ]; then
    echo "Building for a tag. CI_COMMIT_TAG: $CI_COMMIT_TAG"
    BUILD_VERSION="$CI_COMMIT_TAG"
  else
    echo "Building for a branch. CI_COMMIT_REF_SLUG: $CI_COMMIT_REF_SLUG"
    BUILD_VERSION="$CI_COMMIT_REF_SLUG"
  fi
  BUILD_VERSION="$BUILD_VERSION-build$CI_PIPELINE_IID-$CI_COMMIT_SHORT_SHA-"
  echo "Building in a CI pipeline. Setting BUILD_VERSION: $BUILD_VERSION."
fi

DATE=$(date +%F-%H%M)
MENDER_ARTIFACT_NAME="$MENDER_ARTIFACT_BASE_NAME-$BUILD_VERSION$DATE"
MENDER_DEPLOY_IMAGE_NAME="$MENDER_ARTIFACT_BASE_NAME-mender-$BUILD_VERSION$DATE"

# Converting image to Mender artifact
echo -e "\n\nPreparing Mender Convert environment...\n"

# Prepare build directories
BUILD_DIRECTORY="build/$BOARD_NAME"
BUILD_INPUT_DIRECTORY="$BUILD_DIRECTORY/input"
BUILD_LOGS_DIRECTORY="$BUILD_DIRECTORY/logs"
BUILD_DEPLOY_DIRECTORY="$BUILD_DIRECTORY/deploy"

# Prepare input disk image path
BUILD_INPUT_DISK_IMAGE_PATH="$BUILD_DIRECTORY/base-image/$BASE_IMAGE_FILENAME"

# Copy Mender configuration to input build directory
echo -e "\n\nImporting board Mender configuration...\n"
mkdir -p $BUILD_INPUT_DIRECTORY
BUILD_INPUT_CONFIG_PATH=$BUILD_INPUT_DIRECTORY/mender_image_config
cp $MENDER_IMAGE_CONFIG $BUILD_INPUT_CONFIG_PATH
# Configure final deploy image name
echo -e "\nDEPLOY_IMAGE_NAME=$MENDER_DEPLOY_IMAGE_NAME" >> $BUILD_INPUT_CONFIG_PATH

# Copy Mender rootfs overlay to input build directory
echo -e "\n\nImporting board Mender rootfs overlay...\n"
BUILD_INPUT_ROOTFS_OVERLAY_PATH="$BUILD_INPUT_DIRECTORY/rootfs_overlay"
mkdir -p $BUILD_INPUT_ROOTFS_OVERLAY_PATH
cp -ar $MENDER_IMAGE_ROOTFS_OVERLAY/* $BUILD_INPUT_ROOTFS_OVERLAY_PATH/
echo "$MENDER_ARTIFACT_NAME" > $BUILD_INPUT_ROOTFS_OVERLAY_PATH/image_version

# Create a unique log file so we can run multiple containers in parallel
LOG_FILENAME="convert.log.$(date +%s)-$$"
mkdir -p $BUILD_LOGS_DIRECTORY
BUILD_LOG_FILE_PATH="$BUILD_LOGS_DIRECTORY/$LOG_FILENAME"
echo "using log file at: $BUILD_LOG_FILE_PATH"

# Link our deploy directory in mender-convert
mkdir -p $BUILD_DEPLOY_DIRECTORY
rm -rf 3rdparty/mender-convert/deploy
ln -s $(pwd)/$BUILD_DEPLOY_DIRECTORY 3rdparty/mender-convert/deploy

# Run Mender-convert
echo -e "\n\nConverting image to Mender format...\n"

# Mender-convert needs to run from its directory
cd 3rdparty/mender-convert

# Configure build paths relative to mender-convert directory
MENDER_CONVERT_INPUT_DISK_IMAGE_PATH="../../$BUILD_INPUT_DISK_IMAGE_PATH"
MENDER_CONVERT_INPUT_CONFIG_PATH="../../$BUILD_INPUT_CONFIG_PATH"
MENDER_CONVERT_INPUT_ROOTFS_OVERLAY_PATH="../../$BUILD_INPUT_ROOTFS_OVERLAY_PATH"

# Set MENDER_ARTIFACT_NAME env variable used by mender-convert
export MENDER_ARTIFACT_NAME=$MENDER_ARTIFACT_NAME

# Set MENDER_CONVERT_LOG_FILE env variable used by mender-convert
export MENDER_CONVERT_LOG_FILE="../../$BUILD_LOG_FILE_PATH"

# Start conversion
./mender-convert \
  --disk-image $MENDER_CONVERT_INPUT_DISK_IMAGE_PATH \
  --config $MENDER_CONVERT_INPUT_CONFIG_PATH \
  --overlay $MENDER_CONVERT_INPUT_ROOTFS_OVERLAY_PATH

cd ../..

# Change build owner back to project directory owner and group
PROJECT_OWNER=$(stat -c '%U' $(pwd))
PROJECT_GROUP=$(stat -c '%G' $(pwd))
chown -R $PROJECT_OWNER:$PROJECT_GROUP build

# Print final deploy size
echo -e "\n\nFinal deploy size:"
du -h --apparent-size $BUILD_DEPLOY_DIRECTORY/*
echo -e "Final deploy size (sparse file allocation size):"
du -h $BUILD_DEPLOY_DIRECTORY/*