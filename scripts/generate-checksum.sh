#!/bin/bash

DIR_PATH=$1
FILE_PATH=$(find $DIR_PATH/*.img*)
CHECKSUM_PATH=$(dirname $FILE_PATH)/$(basename $FILE_PATH).sha256sum
sha256sum $FILE_PATH > $CHECKSUM_PATH
cat $CHECKSUM_PATH