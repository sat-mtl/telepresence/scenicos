#!/bin/bash

AUTHORS="./AUTHORS.md"
if ! [ -f "$AUTHORS" ]
then
echo "no authors file found, exiting"
exit
fi

# order by number of commits
git log --format='%aN' | \
    sort | \
    uniq -c | sort -bgr | \
    sed 's/\ *[0-9]*\ /\* /' > ${AUTHORS}
