#!/bin/bash

source /board_config

# disable apt questions
export DEBIAN_FRONTEND=noninteractive

# configure hostname
echo -e "\n\nConfiguring hostname...\n"
echo $HOSTNAME > /etc/hostname

cat << EOF > /etc/hosts
127.0.0.1   localhost
127.0.1.1   $HOSTNAME
::1         localhost ip6-localhost ip6-loopback
ff02::1     ip6-allnodes
ff02::2     ip6-allrouters
EOF

# configure fstab
echo -e "\n\nConfiguring fstab...\n"
cat << EOF > /etc/fstab
LABEL=scenic-root / ext4 errors=remount-ro 0 1
LABEL=scenic-efi /boot/efi vfat umask=0077 0 1
EOF

# configure locales
echo -e "\n\nConfiguring locales...\n"
apt-get -y install locales
LANG=en_US.UTF-8
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
dpkg-reconfigure -f noninteractive locales
update-locale LANG=$LANG

# configure timezone
echo -e "\n\nConfiguring timezone...\n"
apt-get -y install tzdata
ln --force --symbolic /usr/share/zoneinfo/America/Montreal /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

# configure repositories and upgrade
echo -e "\n\nConfiguring extra repositories and upgrading...\n"
cat << EOF > /etc/apt/sources.list
deb http://archive.ubuntu.com/ubuntu focal main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu focal-updates main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu focal-security main restricted universe multiverse
EOF
apt-get update
apt-get -y dist-upgrade

# install kernel
echo -e "\n\nInstalling kernel...\n"
apt-get -y install \
  linux-image-$KERNEL_VERSION \
  linux-headers-$KERNEL_VERSION \
  linux-modules-$KERNEL_VERSION \
  intel-microcode \
  amd64-microcode \
  linux-firmware \
  firmware-ath9k-htc \
  firmware-b43-installer

# install bootloader
echo -e "\n\nConfiguring bootloader...\n"
apt-get -y install grub-efi-amd64 lsb-release init
apt-get -y remove grub-pc-bin
cat << EOF > /etc/default/grub
GRUB_DEFAULT=0
GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR=\`lsb_release -i -s 2> /dev/null || echo Debian\`
GRUB_CMDLINE_LINUX_DEFAULT="threadirqs"
GRUB_CMDLINE_LINUX=""
GRUB_TERMINAL_OUTPUT=console
EOF
rm /etc/grub.d/30_os-prober # disable os prober causing problems in chroot
grub-install --target=x86_64-efi --efi-directory=/boot/efi --removable --no-nvram
update-grub

# install addtionnal packages
# cloud-guest-utils required by mender-grow-data to grow data partition at boot using growpart
# fdisk is required by growpart
# avahi-daemon is needed by ndi2shmdata
echo -e "\n\nInstalling additonnal packages...\n"
apt-get -y --no-install-recommends install \
    cloud-guest-utils fdisk \
    dialog \
    screen nano less htop \
    avahi-daemon \
    sudo ca-certificates wget curl gnupg apt-transport-https zip unzip xz-utils \
    lshw pciutils usbutils

# create admin user
echo -e "\n\nCreating user...\n"
useradd $USERNAME -s /bin/bash -m
echo $USERNAME:$USER_PASSWORD | chpasswd
usermod -aG sudo $USERNAME

# configure network
echo -e "\n\nConfiguring networking...\n"
apt-get -y install --no-install-recommends \
  ethtool bridge-utils iproute2 iputils-ping traceroute mtr-tiny nmap tcpdump net-tools iftop \
  network-manager \
  network-manager-pptp \
  wpasupplicant crda \
  modemmanager mobile-broadband-provider-info \
  openssh-server
cat << EOF > /etc/network/interfaces
auto lo
iface lo inet loopback
EOF
sed -i 's/^managed=false/managed=true/' /etc/NetworkManager/NetworkManager.conf
cat << EOF > /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf
[keyfile]
unmanaged-devices=*,except:type:wifi,except:type:gsm,except:type:cdma,except:type:ethernet
EOF

# configure ssh service
echo -e "\n\nEnabling SSH service...\n"
systemctl enable ssh
 
# install graphic drivers
# acpid is used by NVIDIA driver
echo -e "\n\nInstalling graphic drivers...\n"
apt-get -y install --no-install-recommends \
    intel-gpu-tools \
    xserver-xorg-video-qxl \
    xserver-xorg-video-intel \
    acpid \
    nvidia-driver-525=$NVIDIA_VERSION-0ubuntu0.20.04.3 \
    nvidia-utils-525=$NVIDIA_VERSION-0ubuntu0.20.04.3 \
    xserver-xorg-video-nvidia-525=$NVIDIA_VERSION-0ubuntu0.20.04.3

# patch nvidia driver to remove NVENC maximum number of sessions
echo -e "\n\Patching NVENC sessions limit...\n"
cd /opt
wget https://github.com/keylase/nvidia-patch/archive/refs/heads/master.zip -O nvidia-patch.zip
unzip nvidia-patch.zip
cd nvidia-patch-master
./patch.sh -d $NVIDIA_VERSION

# install Magewell Pro Capture driver
echo -e "\n\nInstalling Magewell Pro Cature driver...\n"
apt-get -y install --no-install-recommends \
  make \
  gcc \
  dkms
cd /opt
wget https://www.magewell.com/files/drivers/ProCaptureForLinux_4236.tar.gz
tar xvf ProCaptureForLinux_4236.tar.gz
rm ProCaptureForLinux_4236.tar.gz
cd ProCaptureForLinux_4236
# patch Magewell installation script to hardcode kernel version 
# this allow to run installer under chroot which probably does not run identical kernel
cat << EOF > mwcap-dkms-install_kernel-version.patch
94c94
<     dkms build -m \${MODULE_NAME} -v \${MODULE_VERSION}
---
>     dkms build -m \${MODULE_NAME} -v \${MODULE_VERSION} -k $KERNEL_VERSION
101c101
<     dkms install -m \${MODULE_NAME} -v \${MODULE_VERSION}
---
>     dkms install -m \${MODULE_NAME} -v \${MODULE_VERSION} -k $KERNEL_VERSION
173c173
< KERNEL_BASE="/lib/modules/\`uname -r\`"
---
> KERNEL_BASE="/lib/modules/$KERNEL_VERSION"
200c200
< KERNEL_STR=\`uname -r\`
---
> KERNEL_STR=$KERNEL_VERSION
EOF
patch scripts/mwcap-dkms-install.sh mwcap-dkms-install_kernel-version.patch
./dkms-install.sh

# install Magewell Eco Capture driver
echo -e "\n\nInstalling Magewell Eco Cature driver...\n"
apt-get -y install --no-install-recommends \
  make \
  gcc
cd /opt
wget https://www.magewell.com/files/drivers/EcoCaptureForLinuxX86_1.4.89.tar.gz
tar xvf EcoCaptureForLinuxX86_1.4.89.tar.gz
rm EcoCaptureForLinuxX86_1.4.89.tar.gz
cd EcoCaptureForLinuxX86_1.4.89
# patch Magewell installation scripts to hardcode kernel version 
# this allow to run installer under chroot which probably does not run identical kernel
cat << EOF > mweco-install_kernel-version.patch
116c116
<     echo_string_nonewline "Building module for kernel \`uname -r\` ... "
---
>     echo_string_nonewline "Building module for kernel $KERNEL_VERSION ... "
160c160
<     \$DEPMOD -a
---
>     \$DEPMOD $KERNEL_VERSION -a
236c236
< KERNEL_BASE="/lib/modules/\`uname -r\`"
---
> KERNEL_BASE="/lib/modules/$KERNEL_VERSION"
255c255
< KERNEL_STR=\`uname -r\`
---
> KERNEL_STR=$KERNEL_VERSION
295c295
<         \$MODPROBE MWEcoCapture
---
>         test 1 # Override module loading in chroot installation
EOF
cat << EOF > driver-makefile_kernel-version.patch
26c26
< KERNELDIR ?= /lib/modules/\$(shell uname -r)/build
---
> KERNELDIR ?= /lib/modules/$KERNEL_VERSION/build
EOF
patch scripts/mweco-install.sh mweco-install_kernel-version.patch
patch driver/Makefile driver-makefile_kernel-version.patch
./install.sh

# install Docker
echo -e "\n\nInstalling Docker...\n"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
apt-get -y update
apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
# allow user to run docker
usermod -aG docker $USERNAME

# install Nvidia Container Toolkit
echo -e "\n\nInstalling Nvidia Container Toolkit...\n"
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg
curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
  sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
  tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
apt-get -y update
apt-get -y install nvidia-docker2

# configure desktop environnement
echo -e "\n\nConfiguring desktop environnement...\n"
apt-get -y install --no-install-recommends \
    xorg lightdm openbox feh mesa-utils arandr \
    lxde-common lxde-icon-theme lxpanel lxterminal lxsession-logout pcmanfm xdg-utils \
    mousepad gnome-screenshot xarchiver \
    network-manager-gnome \
    network-manager-openvpn-gnome
# configure arandr persistence
mkdir -p /home/$USERNAME/.screenlayout
touch /home/$USERNAME/.screenlayout/screenlayout.sh
chown $USERNAME:$USERNAME /home/$USERNAME/.screenlayout/screenlayout.sh
chmod +x /home/$USERNAME/.screenlayout/screenlayout.sh
# configure autologin
cat << EOF > /etc/lightdm/lightdm.conf.d/10-autologin.conf
[Seat:*]
autologin-user=scenicbox
autologin-session=openbox
EOF
systemctl enable lightdm
# configure openbox
sed -i 's/<number>4<\/number>/<number>1<\/number>/' /etc/xdg/openbox/rc.xml
# configure autostart
cat << EOF > /etc/xdg/openbox/autostart
/home/$USERNAME/.screenlayout/screenlayout.sh &
xhost +local:root # allow all connections from local processes (to allow GUI apps running in Docker)
xset s off # disable screen blanking after 10 minutes
xset -dpms # disable monitor energy saving
feh --bg-fill /opt/sat/wallpapers/scenic_wallpaper_v4.png &
lxpanel &
nm-applet &
EOF
# configure panel
mkdir -p /home/$USERNAME/.config/lxpanel/default/panels/
cat << EOF > /home/$USERNAME/.config/lxpanel/default/panels/panel 
Global {
  edge=bottom
  allign=left
  margin=0
  widthtype=percent
  width=100
  height=26
  transparent=1
  tintcolor=#000000
  alpha=255
  setdocktype=1
  setpartialstrut=1
  usefontcolor=1
  fontcolor=#ffffff
  usefontsize=0
  fontsize=10
  background=0
  backgroundfile=/usr/share/lxpanel/images/background.png
  autohide=1
  heightwhenhidden=0
}
Plugin {
  type=space
  Config {
    Size=2
  }
}
Plugin {
  type=menu
  Config {
    image=/usr/share/lxde/images/lxde-icon.png
    system {
    }
    separator {
    }
    item {
      command=run
    }
    separator {
    }
    item {
      image=gnome-logout
      command=logout
    }
  }
}
Plugin {
  type=launchbar
  Config {
    Button {
      id=lxterminal.desktop
    }
    Button {
      id=scenic.desktop
    }
  }
}
Plugin {
  type=space
  Config {
    Size=4
  }
}
Plugin {
  type=wincmd
  Config {
    Button1=iconify
    Button2=shade
  }
}
Plugin {
  type=space
  Config {
    Size=4
  }
}
Plugin {
  type=pager
  Config {
  }
}
Plugin {
  type=space
  Config {
    Size=4
  }
}
Plugin {
  type=taskbar
  expand=1
  Config {
    tooltips=1
    IconsOnly=0
    AcceptSkipPager=1
    ShowIconified=1
    ShowMapped=1
    ShowAllDesks=0
    UseMouseWheel=1
    UseUrgencyHint=1
    FlatButton=0
    MaxTaskWidth=150
    spacing=1
  }
}
Plugin {
  type=cpu
  Config {
  }
}
Plugin {
  type=tray
  Config {
  }
}
Plugin {
  type=xkb
  Config {
    Model=pc105
    LayoutsList=ca,us
    VariantsList=,
    ToggleOpt=grp:shift_caps_toggle
    DisplayType=1
    KeepSysLayouts=0
    FlagSize=1
  }
}
Plugin {
  type=dclock
  Config {
    ClockFmt=%R
    TooltipFmt=%A %x
    BoldFont=0
    IconOnly=0
    CenterText=0
  }
}
EOF
# configure logout button
cat << EOF > /home/$USERNAME/.config/lxpanel/default/config 
[Command]
Logout=lxsession-logout
EOF

# install Chromium
# Download raw builds of Chromium and chromedriver
#   Currently using Chromium and chromedriver version 107.0.5304.110.
#   Follow instructions to find branch_base_commit associated with chromium version,
#     https://www.chromium.org/getting-involved/download-chromium/#downloading-old-builds-of-chrome-chromium
#       current_version > branch_base_commit
#       107.0.5304.110 > 1047731

mkdir -p /opt
curl -o /opt/chrome-linux.zip "https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Linux_x64%2F1047731%2Fchrome-linux.zip?generation=1663284576100523&alt=media"
unzip /opt/chrome-linux.zip -d /opt/
rm /opt/chrome-linux.zip
ln -s /opt/chrome-linux/chrome /usr/bin/chromium-browser
update-alternatives --install /usr/bin/x-www-browser x-www-browser /usr/bin/chromium-browser 20
update-alternatives --install /usr/bin/gnome-www-browser gnome-www-browser /usr/bin/chromium-browser 20

# install X-AIR-Edit
mkdir -p /opt/X-AIR-Edit
cd /opt/X-AIR-Edit
apt-get install --no-install-recommends -y libcurl4-gnutls-dev
wget https://mediadl.musictribe.com/download/software/behringer/XAIR/X-AIR-Edit_LINUX_1.7.tar.gz
tar xvf X-AIR-Edit_LINUX_1.7.tar.gz
rm X-AIR-Edit_LINUX_1.7.tar.gz

# configure custom banner
echo -e "\n\nConfiguring custom banner motd...\n"
apt-get -y install --no-install-recommends \
  neofetch
cat << EOF > /etc/update-motd.d/00-header
#!/bin/sh
neofetch --off --color_blocks off
EOF
rm /etc/update-motd.d/10-help-text
rm /etc/update-motd.d/50-motd-news
systemctl disable motd-news.service

# configure video4linux tools
echo -e "\n\nInstalling video4linux...\n"
apt-get -y install --no-install-recommends \
  v4l-utils \
  qv4l2

# configure alsa tools
echo -e "\n\nInstalling alsa...\n"
apt-get -y install --no-install-recommends \
  alsa-base \
  alsa-utils \
  alsa-tools \
  alsa-firmware-loaders \
  alsa-tools-gui

# configure jack tools
echo -e "\n\nInstalling jack...\n"
apt-get install -y --no-install-recommends \
  debconf-utils
echo "jackd2 jackd/tweak_rt_limits boolean true" | debconf-set-selections
apt-get -y install --no-install-recommends \
  qjackctl \
  jaaa \
  ebumeter \
  zita-ajbridge \
  meterbridge \
  meterec
# allow user run realtime applications
usermod -aG audio $USERNAME
echo -e "\n\nAdding Jack systemd user unit...\n"
mkdir -p /home/$USERNAME/.config/systemd/user
cat << EOF > /home/$USERNAME/.config/systemd/user/jack@.service
[Unit]
Description=JACK server using %i.conf profile
Documentation=man:jackd(1)
After=sound.target local-fs.target

[Service]
TimeoutStopSec=3s
Type=simple
EnvironmentFile=-/etc/jack/%i.conf
EnvironmentFile=-%h/.config/jack/%i.conf
ExecStart=/usr/bin/jackd \$JACK_OPTIONS -d \$DRIVER \$DEVICE \$DRIVER_SETTINGS
LimitRTPRIO=95
LimitRTTIME=infinity
LimitMEMLOCK=infinity
# Caution: use on memory-limited devices only
# OOMScoreAdjust=-1000

[Install]
WantedBy=default.target
EOF
echo -e "\n\nAdding Jack default configuration...\n"
mkdir -p /home/$USERNAME/.config/jack
cat << EOF > /home/$USERNAME/.config/jack/$USERNAME.conf
# Start using "systemctl --user start jack@scenicbox"
# Follow logs of jackd2 with "journalctl --user --follow --unit jack@scenicbox"
# Base configuration using dummy device
#
# The name of the JACK server
JACK_DEFAULT_SERVER="default"
# Options to JACK (e.g. -m, -n, -p, -r, -P, -t, -C, -u, -v)
JACK_OPTIONS="--realtime"
# Audio backend (e.g. alsa, dummy, firewire, netone, oss, portaudio)
DRIVER="dummy"
# Device name (used by the audio backend)
DEVICE=""
# Specific settings for the audio backend in use
DRIVER_SETTINGS="--capture 64 --playback 64 --rate 48000 --period 1024"
EOF
echo -e "\n\nEnabling Jack systemd user unit...\n"
mkdir -p /home/$USERNAME/.config/systemd/user/default.target.wants
ln -s /home/$USERNAME/.config/systemd/user/jack@.service /home/$USERNAME/.config/systemd/user/default.target.wants/jack@$USERNAME.service
chown -R 1000:1000 /home/$USERNAME/

# install KXStudio Catia Jack patchbay and cadence-jackmeter
wget https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos_11.1.0_all.deb && \
dpkg -i kxstudio-repos_11.1.0_all.deb && \
rm kxstudio-repos_11.1.0_all.deb && \
apt-get update && \
apt-get install --no-install-recommends -y \
  catia \
  cadence-tools
systemctl 

# configure rtirq threaded irqs for snd and usb drivers
echo -e "\n\nConfiguring rtirq...\n"
cd /opt
curl -fsSL --output rtirq_20220923-44.2_all.deb "https://launchpad.net/~rncbc/+archive/ubuntu/apps-focal/+files/rtirq_20220923-44.2_all.deb"
dpkg -i rtirq_20220923-44.2_all.deb

# configure performance cpu governor
echo -e "\n\nConfiguring service setting performance governor at boot...\n"
apt-get -y install linux-tools-$KERNEL_VERSION
cat << EOF > /etc/systemd/system/cpupower.service
[Unit]
Description=Set CPU power governor to performance
[Service]
Type=oneshot
ExecStart=/usr/bin/cpupower -c all frequency-set -g performance
[Install]
WantedBy=multi-user.target
EOF
systemctl disable ondemand
systemctl daemon-reload
systemctl enable cpupower.service

# remove documentation
echo -e "\n\nRemoving documentation...\n"
find /usr/share/doc -depth -type f ! -name copyright|xargs rm || true
find /usr/share/doc -empty|xargs rmdir || true
rm -rf /usr/share/man /usr/share/groff /usr/share/info /usr/share/lintian /usr/share/linda /var/cache/man /usr/share/man/*

# clean apt cache
echo -e "\n\nCleaning apt cache...\n"
apt-get clean
apt-get autoclean
apt-get autoremove -y --purge
rm -rf /var/lib/{apt,cache,log}/
rm -rf /var/lib/apt/lists/*

# display used space
echo -e "\n\nImage build completed.\n"
echo -e "Used space:\n"
df -h