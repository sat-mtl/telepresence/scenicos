#!/bin/bash

function print_usage() {
  cat << EOF
start-image-qemu.sh

Start image using QEMU.

Usage: $0 IMAGE_PATH

EOF
}

# Parse image path argument
IMAGE_PATH="${1}"

if [ -z "${IMAGE_PATH}" ]; then
  echo -e "You need to pass IMAGE_PATH path as argument.\n"
  print_usage
  exit 1
fi

echo "Running $(basename $0): $@"

TEST_IMAGE=$IMAGE_PATH
SSH_PORT="2222"
MAC_ADDRESS="52:54:00:11:22:33"
OVMF_CODE="/usr/share/OVMF/OVMF_CODE_4M.fd"

VIRT_DISK_DEVICE="
    -device virtio-blk-pci,drive=hd,bootindex=1 \
    -drive file=$TEST_IMAGE,id=hd,if=none,format=raw,cache=none"
SCSI_DISK_DEVICE="
    -device virtio-scsi-pci,id=scsi \
    -device scsi-hd,drive=hd,bootindex=1 \
    -drive file=$TEST_IMAGE,id=hd,if=none,format=raw,cache=none"
NVME_DISK_DEVICE="
    -device nvme,serial=deadbeef,drive=hd,bootindex=1 \
    -drive file=$TEST_IMAGE,id=hd,if=none,format=raw,cache=none"
USB_DISK_DEVICE="
    -device qemu-xhci \
    -device usb-storage,removable=true,drive=hd,bootindex=1 \
    -drive file=$TEST_IMAGE,id=hd,if=none,format=raw,cache=none"
DISK_DEVICE=$VIRT_DISK_DEVICE

qemu-system-x86_64 \
    -enable-kvm \
    -cpu host \
    -smp cores=4 \
    -m 4096 \
    -machine q35 \
    -device qxl-vga,xres=1920,yres=1080 \
    -audiodev pa,id=pulseaudio,in.fixed-settings=off,out.fixed-settings=off,in.mixing-engine=off,out.mixing-engine=off \
    -device ich9-intel-hda \
    -device hda-duplex,audiodev=pulseaudio \
    -drive if=pflash,format=raw,unit=0,file="$OVMF_CODE",readonly=on \
    $DISK_DEVICE \
    -device virtio-net-pci,netdev=net0,mac=$MAC_ADDRESS \
    -netdev type=user,id=net0,hostfwd=tcp::$SSH_PORT-:22