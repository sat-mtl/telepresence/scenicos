#!/bin/bash

env 
    AWS_ACCESS_KEY_ID=${SCENICOS_CI_S3_AWS_ACCESS_KEY_ID} \
    AWS_SECRET_ACCESS_KEY=${SCENICOS_CI_S3_AWS_SECRET_ACCESS_KEY} \
    aws s3 cp \
    s3://${SCENICOS_CI_TMP_S3_BUCKET}/${CI_PROJECT_NAME}/${CI_COMMIT_REF_SLUG}/boards/x86-64/base-image/ \
    boards/x86-64/base-image/ \
    --recursive
cat boards/x86-64/base-image/scenicos-focal-amd64.img.gz.sha256sum
echo "Verifying checksum..."
sha256sum -c boards/x86-64/base-image/scenicos-focal-amd64.img.gz.sha256sum
echo "Uncompressing..."
unpigz boards/x86-64/base-image/scenicos-focal-amd64.img.gz