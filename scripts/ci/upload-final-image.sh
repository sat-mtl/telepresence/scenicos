#!/bin/bash

echo "Generating checksum..."
scripts/generate-checksum.sh build/x86-64/deploy/
env 
    AWS_ACCESS_KEY_ID=${SCENICOS_CI_S3_AWS_ACCESS_KEY_ID} \
    AWS_SECRET_ACCESS_KEY=${SCENICOS_CI_S3_AWS_SECRET_ACCESS_KEY} \
    aws s3 cp \
    build/x86-64/deploy/ \
    s3://${SCENICOS_RELEASE_S3_BUCKET}/ \
    --recursive