#!/bin/bash

# Create a GitLab release using GitLab Release CLI if building for a tag
# Parses image and checksum download URLs from stdin, one link per line.

# Use following env vars
# CI_SERVER_URL
# CI_JOB_TOKEN
# CI_PROJECT_ID
# CI_COMMIT_TAG
# CI_COMMIT_SHA

mapfile -t DOWNLOAD_URLS_ARRAY

IMAGE_URL=${DOWNLOAD_URLS_ARRAY[0]}
CHECKSUM_URL=${DOWNLOAD_URLS_ARRAY[1]}

RELEASE_IMAGE_NAME="image-x86-64"
RELEASE_IMAGE_FILEPATH="/image-x86-64"
RELEASE_IMAGE_URL=$IMAGE_URL

RELEASE_CHECKSUM_NAME="sha256sum-x86-64"
RELEASE_CHECKSUM_FILEPATH="/sha256sum-x86-64"
RELEASE_CHECKSUM_URL=$CHECKSUM_URL

if [ ! -z $CI_COMMIT_TAG ];
then
    echo "Built for a tag, creating a GitLab release..."
    release-cli create \
        --assets-link "{\"name\":\"$RELEASE_IMAGE_NAME\",\"filepath\":\"$RELEASE_IMAGE_FILEPATH\",\"url\":\"$RELEASE_IMAGE_URL\",\"link_type\":\"image\"}" \
        --assets-link "{\"name\":\"$RELEASE_CHECKSUM_NAME\",\"filepath\":\"$RELEASE_CHECKSUM_FILEPATH\",\"url\":\"$RELEASE_CHECKSUM_URL\",\"link_type\":\"image\"}"
else
    echo "Not built for a tag, skipping GitLab release creation."
fi