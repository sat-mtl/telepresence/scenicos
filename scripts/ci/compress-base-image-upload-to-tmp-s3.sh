#!/bin/bash

echo "Compressing..."
pigz boards/x86-64/base-image/scenicos-focal-amd64.img
ls -alh boards/x86-64/base-image/
echo "Generating checksum..."
scripts/generate-checksum.sh boards/x86-64/base-image/
env \
    AWS_ACCESS_KEY_ID=${SCENICOS_CI_S3_AWS_ACCESS_KEY_ID} \
    AWS_SECRET_ACCESS_KEY=${SCENICOS_CI_S3_AWS_SECRET_ACCESS_KEY} \
    aws s3 cp \
        boards/x86-64/base-image/ \
        s3://${SCENICOS_CI_TMP_S3_BUCKET}/${CI_PROJECT_NAME}/${CI_COMMIT_REF_SLUG}/boards/x86-64/base-image/ \
        --recursive