#!/bin/bash

DEBOOTSTRAP_CACHE_DIR=$(pwd)/debootstrap-cache
DEBOOTSTRAP_CHROOT_DIR=debootstrap-chroot
LODEV=""

function print_usage() {
  cat << EOF
build-base-image.sh

Create a bootable image using debootstrap using parameters from 
boards/$BOARD_NAME/board_config passed as argument.

Usage: $0 BOARD_NAME

EOF
}

cleanup_mountpoints() {
  set +e
  umount "$LODEV"p1
  umount "$LODEV"p2 -l
  losetup -d "$LODEV"
}

# Parse board name path argument
BOARD_NAME="${1}"

# Parse board configuration path argument
BOARD_CONFIG_FILE="${1}"

if [ -z "${BOARD_NAME}" ]; then
  echo -e "You need to pass BOARD_NAME path as argument.\n"
  print_usage
  exit 1
fi

BOARD_CONFIG_FILE=boards/$BOARD_NAME/board_config
if [ ! -e ${BOARD_CONFIG_FILE} ]; then
  echo -e "Cannot open file: ${BOARD_CONFIG_FILE}\n"
  exit 1
fi

echo "Running $(basename $0): $@"

# Load board configuration
source $BOARD_CONFIG_FILE

set -e

# Create build directory
BASE_IMAGE_PATH="$(pwd)/build/$BOARD_NAME//base-image/$BASE_IMAGE_FILENAME"
mkdir -p $(dirname $BASE_IMAGE_PATH)
DEBOOTSTRAP_CACHE_DIR="$(pwd)/build/$BOARD_NAME/debootstrap-cache"
mkdir -p $DEBOOTSTRAP_CACHE_DIR

# Make a blank .img of the desired size
truncate -s $BASE_IMAGE_SIZE $BASE_IMAGE_PATH

# Create partition layout in the blank image
sgdisk --clear \
  --new 1::+512M --typecode=1:ef00 --change-name=1:'EFI System' \
  --new 2::-0 --typecode=2:8300 --change-name=2:'Linux root filesystem' \
  $BASE_IMAGE_PATH -g

# Go to unmount and clean loopback devices in case of error
trap cleanup_mountpoints EXIT

# Create a loop device based on the partitioned image
# Manually verify next free loopback device, then create device node using mknod 
# to allow for losetup to work inside priviledged Docker container
LODEV="$(losetup -f)"
LOMAJ="$(echo "$LODEV" | sed -E 's/.*[^0-9]*?([0-9]+)$/\1/')"
[[ -b "$LODEV" ]] || mknod "$LODEV" b 7 "$LOMAJ"
losetup -P "$LODEV" $BASE_IMAGE_PATH

# Format the partitions in the image
mkfs.fat -F32 -n scenic-efi "$LODEV"p1
mkfs.ext4 -F -L scenic-root "$LODEV"p2

# Mount the formatted partitions in the image
DEBOOTSTRAP_CHROOT_DIR="$(pwd)/build/$BOARD_NAME/chroot"
mkdir -p $DEBOOTSTRAP_CHROOT_DIR
mount -o rw "$LODEV"p2 $DEBOOTSTRAP_CHROOT_DIR
mkdir -p $DEBOOTSTRAP_CHROOT_DIR/boot/efi
mount -o rw "$LODEV"p1 $DEBOOTSTRAP_CHROOT_DIR/boot/efi

# Run debootstrap against the mounted file system
debootstrap \
    --cache-dir=$DEBOOTSTRAP_CACHE_DIR \
    --arch=$BASE_IMAGE_ARCH \
    --variant=minbase \
    --include systemd \
    --components=main,$BASE_IMAGE_DISTRO-updates,universe,restricted \
    $BASE_IMAGE_DISTRO \
    $DEBOOTSTRAP_CHROOT_DIR \
    http://archive.ubuntu.com/ubuntu/

# Mount enough of your host system in to the image to allow you to run apt commands
mount --bind /dev $DEBOOTSTRAP_CHROOT_DIR/dev/
mount --bind /sys $DEBOOTSTRAP_CHROOT_DIR/sys/
mount --bind /proc $DEBOOTSTRAP_CHROOT_DIR/proc/
mount --bind /dev/pts $DEBOOTSTRAP_CHROOT_DIR/dev/pts

# Copy files to chroot
mkdir -p $DEBOOTSTRAP_CHROOT_DIR/opt/sat/wallpapers
cp files/opt/sat/wallpapers/scenic_wallpaper_v4.png $DEBOOTSTRAP_CHROOT_DIR/opt/sat/wallpapers/scenic_wallpaper_v4.png
cp files/usr/local/bin/* $DEBOOTSTRAP_CHROOT_DIR/usr/local/bin/
mkdir -p $DEBOOTSTRAP_CHROOT_DIR/opt/sat/icons
cp files/opt/sat/icons/* $DEBOOTSTRAP_CHROOT_DIR/opt/sat/icons/
mkdir -p $DEBOOTSTRAP_CHROOT_DIR/etc/xdg/menus/applications-merged
cp files/etc/xdg/menus/applications-merged/* $DEBOOTSTRAP_CHROOT_DIR/etc/xdg/menus/applications-merged/
mkdir -p $DEBOOTSTRAP_CHROOT_DIR/usr/share/desktop-directories
cp files/usr/share/desktop-directories/* $DEBOOTSTRAP_CHROOT_DIR/usr/share/desktop-directories/
mkdir -p $DEBOOTSTRAP_CHROOT_DIR/usr/share/applications
cp files/usr/share/applications/* $DEBOOTSTRAP_CHROOT_DIR/usr/share/applications/

# Finish image configuration in chroot
BASE_IMAGE_DATE=$(date)
echo $BASE_IMAGE_DATE $DEBOOTSTRAP_CHROOT_DIR/base_image_date
cp $BOARD_CONFIG_FILE $DEBOOTSTRAP_CHROOT_DIR/board_config
cp scripts/script-to-run-in-chroot.sh $DEBOOTSTRAP_CHROOT_DIR
chmod +x $DEBOOTSTRAP_CHROOT_DIR/script-to-run-in-chroot.sh
chroot $DEBOOTSTRAP_CHROOT_DIR ./script-to-run-in-chroot.sh
rm $DEBOOTSTRAP_CHROOT_DIR/script-to-run-in-chroot.sh

# Cleanup 
umount $DEBOOTSTRAP_CHROOT_DIR/dev/pts
umount $DEBOOTSTRAP_CHROOT_DIR/dev/
umount $DEBOOTSTRAP_CHROOT_DIR/sys/
umount $DEBOOTSTRAP_CHROOT_DIR/proc/
cleanup_mountpoints

# Change build owner back to project directory owner and group
PROJECT_OWNER=$(stat -c '%U' $(pwd))
PROJECT_GROUP=$(stat -c '%G' $(pwd))
chown -R $PROJECT_OWNER:$PROJECT_GROUP build

# Print final deploy size
echo -e "\n\nFinal deploy size:"
du -h --apparent-size $BASE_IMAGE_PATH
echo -e "Final deploy size (sparse file allocation size):"
du -h $BASE_IMAGE_PATH