#!/bin/bash

# REAPER setup wizard

REAPER_LATEST_VERSION_API="http://www.cockos.com/reaper/latestversion/?p=linux_x86_64"
REAPER_VERSION_MINOR="74"
REAPER_ARCHIVE_PATH="/tmp/reaper_linux_x86_64.tar.xz"

display_banner() {
    echo -ne "Welcome to REAPER setup\n\n"
    echo -ne "Setup will download and install latest REAPER demo.\n\n"
    echo -ne "You will need to enter sudo password to allow installation and configuration.\n\n"
    echo -ne "By installing, you explicitly accept Cockos Incorporated End User License Agreement for REAPER.\n"
    read -p "Do you want to start setup? Type YES to proceed: " CONFIRMATION
    if [[ ! $CONFIRMATION == "YES" ]]; then 
        echo -e "\nAborting setup."
        read -p "Press any key to quit."
        exit 1
    fi
}

fetch_latest_version_number() {
    REAPER_VERSION_MINOR=$(wget --quiet --output-document=- "$REAPER_LATEST_VERSION_API" | head -n1 | cut -c 3-4)
    REAPER_ARCHIVE_URL="http://reaper.fm/files/6.x/reaper6${REAPER_VERSION_MINOR}_linux_x86_64.tar.xz"
}

install_reaper() {
    echo -e "\nDownloading REAPER archive from $REAPER_ARCHIVE_URL..."
    wget -qO $REAPER_ARCHIVE_PATH $REAPER_ARCHIVE_URL
    echo -e "\nDecompressing REAPER archive..."
    tar xvf $REAPER_ARCHIVE_PATH -C /tmp
    echo -e "\nInstalling REAPER..."
    sudo /tmp/reaper_linux_x86_64/install-reaper.sh --install /opt --integrate-sys-desktop --usr-local-bin-symlink --quiet
    rm $REAPER_ARCHIVE_PATH
    rm -rf /tmp/reaper_linux_x86_64
}

display_banner
fetch_latest_version_number
install_reaper

# Done
echo -e "\nSetup is completed."
read -p "Press any key to quit."