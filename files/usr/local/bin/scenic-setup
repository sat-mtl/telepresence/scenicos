#!/bin/bash

# SCENIC setup wizard

SCENIC_CONTACTS_LIST_URL=
SCENIC_TAG=master
SCENIC_CORE_TAG=master
SCENIC_PROFILE=generic

display_banner() {
    echo -ne "Welcome to SCENIC setup\n\n"
    echo -ne "Setup will download and start SCENIC.\n\n"
    read -p "Do you want to start setup? Type YES to proceed: " CONFIRMATION
    if [[ ! $CONFIRMATION == "YES" ]]; then 
        echo -e "\nAborting setup."
        read -p "Press any key to quit."
        exit 1
    fi
}

select_scenic_tag() {
    # Select Scenic image tag
    echo -e "\nPress enter to download latest release image of Scenic, or enter new image tag to override."
    read -p "scenic tag to deploy [master]: " NEW_SCENIC_TAG
    if [ $NEW_SCENIC_TAG ]; then SCENIC_TAG=$NEW_SCENIC_TAG; fi
}

select_scenic_core_tag() {
    # Select Scenic-core image tag
    echo -e "\nPress enter to download latest release image of Scenic-core, or enter new image tag to override."
    read -p "scenic-core tag to deploy [master]: " NEW_SCENIC_CORE_TAG
    if [ $NEW_SCENIC_CORE_TAG ]; then SCENIC_CORE_TAG=$NEW_SCENIC_CORE_TAG; fi
}

configure_scenic_station() {
    local SCENIC_STATION_CONFIG_SCRIPT="https://gitlab.com/sat-mtl/tools/scenic/scenic/-/raw/$SCENIC_TAG/scripts/configure-scenic-station.sh"
    # Ask if deploying custom Scenic configuration for Scenic Station
    echo -e "\nCustom menus and bundles configuration is needed to run Scenic Station hardware."
    read -p "Press enter to skip custom configuration, or type YES to apply it: " CONFIGURE_SCENIC_STATION
    if [[ $CONFIGURE_SCENIC_STATION == "YES" ]]; then 
        echo -e "\nDownloading Scenic Station config script..."
        wget -O configure-scenic-station.sh $SCENIC_STATION_CONFIG_SCRIPT
        chmod +x configure-scenic-station.sh
        ./configure-scenic-station.sh
    fi
}

configure_nvidia_mosaic() {
    # Ask if configuring NVIDIA Base Mosaic support for multiple GPUs
    echo -e "\nCustom X server configuration is needed to support multiple NVIDIA GPU."
    read -p "Press enter to skip NVIDIA Base Mosaic support configuration, or type YES to apply it: " CONFIGURE_NVIDIA_MOSAIC
    if [[ $CONFIGURE_NVIDIA_MOSAIC == "YES" ]]; then 
        echo -e "\nBacking up existing xorg.conf file if present..."
        TIMESTAMP=$(date +%Y%m%d_%H%M%S)
        sudo mv /etc/X11/xorg.conf /etc/X11/xorg.conf.bak.$TIMESTAMP
        echo -e "\nEnabling NVIDIA Base Mosaic support..."
        sudo nvidia-xconfig --base-mosaic --sli=False --multigpu=False --force-full-composition-pipeline=True
        echo -e "\nPlease restart computer to finish enable NVIDIA Base Mosaic support..."
    fi
}

download_contact_list() {
    mkdir -p ~/.config/sat
    mkdir -p ~/.config/scenic

    echo -e "\nIf you were provided a custom download URL for contact list, enter it here."
    read -p "Press enter to skip contact download, or enter custom URL to download it: " SCENIC_CONTACTS_LIST_URL
    # Download contact list file if we have been provided URL
    if [ $SCENIC_CONTACTS_LIST_URL ]; then
        echo -e "\nDownloading contact list file script..."
        wget -qO ~/.config/sat/contacts.json $SCENIC_CONTACTS_LIST_URL
        cp ~/.config/sat/contacts.json ~/.config/scenic/contacts.json
    fi
}

download_docker_compose_file() {
    echo -e "\nDownloading Docker Compose file..."
    local SCENIC_DOCKER_COMPOSE_URL="https://gitlab.com/sat-mtl/tools/scenic/scenic-core/-/raw/$SCENIC_CORE_TAG/docker-compose.yml"
    wget -O docker-compose.yml $SCENIC_DOCKER_COMPOSE_URL
}

find_gpu_type() {
    /usr/bin/nvidia-smi > /dev/null
    NVIDIA_SMI_RETURN_CODE=$?
    if [[ $NVIDIA_SMI_RETURN_CODE -eq 0 || $NVIDIA_SMI_RETURN_CODE -eq 14 ]]; then
        # nvidia-smi returns 0 when GPU is present
        # nvidia-smi returns 14 when GPU is present, but have corrupted 
        #   i.e.: WARNING: infoROM is corrupted at gpu 0000:02:00.0
        SCENIC_PROFILE=nvidia
        echo -e "\nDetected NVIDIA GPU, using nvidia profile."
    else
        SCENIC_PROFILE=generic
        echo -e "\nNo NVIDIA GPU detected, using generic profile."
    fi
}

stop_running_scenic() {
    echo -e "\nStopping SCENIC..."
    docker compose --profile $SCENIC_PROFILE down
}
download_scenic() {
    echo -e "\nDownloading SCENIC..."
    SCENIC_TAG=$SCENIC_TAG SCENIC_CORE_TAG=$SCENIC_CORE_TAG docker compose --profile $SCENIC_PROFILE pull
}

start_scenic() {
    echo -e "\nStarting SCENIC..."
    SCENIC_TAG=$SCENIC_TAG SCENIC_CORE_TAG=$SCENIC_CORE_TAG PUID=$UID PGID=$GID docker compose --profile $SCENIC_PROFILE up -d
}

display_scenic_info() {
    echo -ne "\n\nConfiguration done.\n"
    echo -ne "\nHere is SCENIC setup:\n\n"
    docker ps
}

cd $HOME
display_banner
select_scenic_tag
select_scenic_core_tag
configure_scenic_station
configure_nvidia_mosaic
download_contact_list
download_docker_compose_file
find_gpu_type
stop_running_scenic
download_scenic
start_scenic
display_scenic_info

# Done
echo -e "\nSetup is completed."
read -p "Press any key to quit."