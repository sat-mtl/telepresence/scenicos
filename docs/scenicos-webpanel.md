# Using ScenicOS Web Panel

ScenicOS Web Panel is a docker image based on [linuxserver.io/webtop](https://docs.linuxserver.io/images/docker-webtop).

It offers a minimal desktop environment served in a Web page using Guacamole remote desktop.

Comes with utilities to set host monitor settings, test Video4Linux capture devices, connect and test host Jack server clients, or connect to Behringer XR18 using X-AIR-Edit mixer.

## Start ScenicOS Web Panel

```bash
docker run \
  --rm \
  -it \
  --name scenic-webpanel \
  --network host \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/New_York \
  -e KEYBOARD=en-us-qwerty \
  --security-opt apparmor:unconfined \
  --volume '/var/run/dbus:/var/run/dbus' \
  --device /dev \
  --volume '/tmp/.X11-unix:/tmp/.X11-unix:ro' \
  --volume '/dev/shm:/dev/shm:rw' \
  --device=/dev/dri:/dev/dri \
  --gpus=all \
  --env="NVIDIA_DRIVER_CAPABILITIES=all" \
  registry.gitlab.com/sat-mtl/distribution/scenicos/scenic-webpanel:master
```

Open your browser to host's IP on port 3000. (i.e http://ip:3000).

## Docker compose

Launching scenic-core and scenic from Docker compose is easy.

Fetch `docker-compose.yml` file from desired branch.

```bash
export SCENICOS_BRANCH=master
wget -N https://gitlab.com/sat-mtl/distribution/scenicos/-/raw/feat/$SCENICOS_BRANCH/docker/scenic-webpanel/docker-compose.yml
```

You need to choose which profile to launch scenic-core, either using Nvidia GPU or generic GPUs (Intel/AMD/QXL).

Launching with Nvidia support :

```bash
COMPOSE_PROFILES=nvidia docker compose up -d
```

Launching for other GPU support:

```bash
COMPOSE_PROFILES=generic docker compose up -d
```

You can also specify which version/tag to use with SCENIC_WEBPANEL_TAG variable. It defaults to use master tag. To start development version override tags to use `develop`.

```bash
COMPOSE_PROFILES=nvidia SCENIC_WEBPANEL_TAG=develop docker compose up -d
```
## Build and push ScenicOS 

```bash
cd docker/scenic-webpanel
DOCKER_BUILDKIT=1 docker build --progress plain -t registry.gitlab.com/sat-mtl/distribution/scenicos/scenic-webpanel:<your-tag> .
docker push registry.gitlab.com/sat-mtl/distribution/scenicos/scenic-webpanel:<your-tag>
```
