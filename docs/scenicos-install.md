# ScenicOS hard drive installation

This guide will go through steps necessary to download ScenicOS, write it to a USB drive, boot from USB drive then install ScenicOS to device hard drive.

You will need a 32GB or bigger USB drive to flash the image to.

After installation to hard drive is complete, and you rebooted without USB drive connected, proceed with [ScenicOS Quick Start guide](docs/scenicos-quick-start.md) to configure Scenic.

## Download ScenicOS image

Download latest ScenicOS image from [release page](https://gitlab.com/sat-mtl/distribution/scenicos/-/releases/permalink/latest).

Download `image-x86-64` and `sha256sum-x86-64` files.

Downloaded image file is named following build and version. `scenicos-<dist>-<board>-<arch>-mender-<version>-build<build-number>-<git-sha>-<date>.img.xz`.

i.e. `scenicos-focal-amd64-x86-64-mender-1.0.0-build133-be323984-2022-08-22-1734.img.xz`

## Install balenaEtcher

We'll use balenaEtcher utility to flash image to USB drive.

 1. Fetch balenaEtcher from https://www.balena.io/etcher.

 2. Unzip balena-etcher-electron-<version>-linux-x64.zip file downloaded.

 3. Double-click on balenaEtcher-<version>-x64.AppImage file to start application.

## Flash image

 1. Connect destination USB drive
 2. In balenaEtcher, choose Flash from file and select image file.
 3. Select target USB drive.
 4. Start Flash. If using an SSD drive, you'll have to confirm to write to an usually large drive.

## First boot

Insert USB drive in device and boot from it

ScenicOS will resize data partition during first boot, this can take several seconds

When ready, every monitors connected to device should display an fullscreen Scenic wallpaper.

Move your mouse cursor to bottom of screen, taskbar panel will appear.

You can click on start menu at bottom left corner to access applications.

## Install ScenicOS to hard drive

Installer will download latest compressed image, decompress on the fly, and write it directly to selected hard drive.

> WARNING
> This will erase everything on selected hard drive.

Launch installer wizard with `scenicos-installer` command or by launching `ScenicOS Installer` entry located in start menu, under `ScenicOS` section.

Select on which hard drive to install.

```
Welcome to ScenicOS installer

Installer will download, decompress and write an image from Internet.
Wizard allows to select to which disk device to install to.
You can then choose to install latest version or provide a custom URL.

Available disk devices list:
Number	Name	Size	Vendor	Model
1	    sda	    28.8G	Kingston	DataTraveler_3.0
2	    nvme0n1 476.9G	SAMSUNG	MZVLB512HBJQ-000H1

Select which disk device number to overwrite: 2
```

Press enter to install latest version.

```
Select which image to download and write.

Default image URL: https://gitlab.com/sat-mtl/distribution/scenicos/-/releases/permalink/latest/downloads/image-x86-64
Press enter to download latest release image, or enter new image URL to override:
```

You will be asked for sudo password as we need permission to write directly to hard drive.

```
Starting writing image "https://gitlab.com/sat-mtl/distribution/scenicos/-/releases/permalink/latest/downloads/image-x86-64" to disk device "sdb"
Enter sudo password to allow raw write access to disk.
[sudo] password for scenicbox: 
```

You will see a progress indicator of copy along with average disk write throughput. Installation writes 26 GB.

```
26340229120 bytes (26 GB, 25 GiB) copied, 508.482 s, 51.8 MB/s
```

Once completed, press any key to quit installer.

```
Installation is completed. Shutdown computer and remove live USB drive.
Press any key to quit.
```

## Reboot into installed OS

You can now shutdown computer currently running off live USB drive.

```sh
sudo shutdown -h now
```

Remove USB drive from computer.

Start computer, ScenicOS will now boot from permanent installation on hard drive.

## Configure Scenic

You can now roceed with [ScenicOS Quick Start guide](docs/scenicos-quick-start.md) to configure Scenic.