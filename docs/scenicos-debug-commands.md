# Useful debug commands for ScenicOS

## Follow Jack2 service logs

```bash
journalctl --user -u jack@scenicbox
```

## Jack2 service status

```bash
systemctl --user status jack@scenicbox
jack_cpu_load
```

## Jack2 service restart

```bash
systemctl --user restart jack@scenicbox
```

## Verify Jack input signal remotely from terminal

```bash
sudo apt-get update
sudo apt-get install jackmeter
jack_meter system:capture_16
```
## Test Jack loopback

Connect microphone on XR18 input 16, and monitor output using headphones on output 1.

```bash
jack_connect system:capture_16 system:playback_1
jack_disconnect system:capture_16 system:playback_1
```

## Display Jack server status using QJackCtl GUI via SSH X11 forwarding

```bash
ssh -X scenicbox@<scenicbox-ip>
qjackctl
```

## Display audio spectrum using JAAA GUI via SSH X11 forwarding

```bash
ssh -X scenicbox@<scenicbox-ip>
jaaa -J &
jack_connect system:capture_16 jaaa:in_1
```

## List ALSA audio playback devices

Find ALSA device name used by Jack.
ALSA device name for Behringer XR18 interface is `hw:CARD=X18XR18,DEV=0`.
Jack would refer to this ALSA device as `hw:X18XR18,0`.

```bash
aplay -L
  hw:CARD=X18XR18,DEV=0
      X18/XR18, USB Audio
      Direct hardware device without any conversions
cat /proc/asound/cards
cat /proc/asound/card7/stream0 
```

## List ALSA audio capture devices

```bash
arecord -L
  hw:CARD=X18XR18,DEV=0
      X18/XR18, USB Audio
      Direct hardware device without any conversions
```

## Open ALSA mixer

```bash
alsamixer
```

## Test ALSA audio playback

You need to stop Jack service before trying to test direct ALSA playback or capture.

```bash
systemctl --user stop jack@scenicbox
speaker-test --device=default --channels=2 --test=wav
speaker-test --device=default:CARD=X18XR18 --channels=8 --test=wav
speaker-test --device=default:CARD=X18XR18 --channels=16 --test=sine --frequency=1000
systemctl --user start jack@scenicbox
```

## Test ALSA audio loopback

You need to stop Jack service before trying to test direct ALSA playback or capture.

```bash
systemctl --user stop jack@scenicbox
arecord --device=hw:PCH,DEV=0 --channels=2 --format=S16_LE --rate=48000 | \
    aplay --device=hw:PCH
arecord --device=plughw:CARD=X18XR18,DEV=0,0 --channels=1 --format=S32_LE --rate=48000 | \
    aplay --device=plughw:CARD=X18XR18,0 --channels=1 --format=S32_LE --rate=48000
systemctl --user start jack@scenicbox
```
## List MIDI devices

```bash
aconnect -i -o
```

## Follow scenic-core docker container logs

```bash
docker logs -f scenic-core
```

## NVENC / NVDEC encoders/decoders usage

```bash
nvidia-smi pmon -c 1
```

## Open monitor configuration remotely via SSH X11 forwarding

```bash
ssh -X scenicbox@<scenicbox-ip>
arandr --randr-display=:0
```

## List all Video4Linux devices

```bash
v4l2-ctl --list-devices
```

## Display Video4Linux devices informations for a device

```bash
v4l2-ctl --device /dev/video0 --all
```

## Test Video4Linux device using V4L2 Test Bench GUI via SSH X11 forwarding

```bash
ssh -X scenicbox@<scenicbox-ip>
qv4l2
```

## Test Video4Linux device from SSH session using GStreamer ascii art video display

```bash
sudo apt update
sudo apt install gstreamer1.0-tools gstreamer1.0-plugins-good
gst-launch-1.0 v4l2src device=/dev/video0 ! videoconvert ! aasink
gst-launch-1.0 v4l2src device=/dev/video1 ! videoconvert ! aasink
gst-launch-1.0 v4l2src device=/dev/video2 ! videoconvert ! aasink
gst-launch-1.0 v4l2src device=/dev/video3 ! videoconvert ! aasink
gst-launch-1.0 v4l2src device=/dev/video4 ! videoconvert ! aasink
```

## Verify Magewell Pro Capture driver status

```bash
mwcap-info -l
```

## Enable Magewel Pro Capture low latency mode

You can only run this command once Scenic have opened the video capture device.

```bash
mwcap-control --video-output-lowlatency on /dev/video0
```

## Flush GStreamer registry cache

This can sometime resolve problem with video capture not opening in Scenic.

```bash
rm .cache/gstreamer-1.0/registry.x86_64.bin
```

## Verify X server OpenGL renderer

```bash
DISPLAY=:0 glxinfo | grep vendor
```

## Test OpenGL rendering

```bash
DISPLAY=:0 glxgears
```


## Remote terminal into scenicbox using Mender Connect

Install mender-cli tool.

```bash
wget https://downloads.mender.io/mender-cli/1.7.0/linux/mender-cli
sudo chmod +x mender-cli
sudo cp mender-cli /usr/local/bin/
```

Login to Mender server.

```bash
mender-cli login
```

```bash
mender-cli terminal <device-id>
su scenicbox # needed to login to scenicbox bash session with all groups activated
```

## Port forward Scenic API to scenicbox using Mender Connect

```bash
mender-cli port-forward <device-id> 8000:8000
```

## Remotely connect to SSH via Mender Connect port forward

```bash
mender-cli port-forward <device-id> 2222:22
ssh -p2222 scenicbox@localhost
```
