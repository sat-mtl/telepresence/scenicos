# ScenicOS Quick Start

This guide will go through steps necessary download and configure latest Scenic software.

You should already installed ScenicOS before by following [ScenicOS hard drive installation guide](docs/scenicos-install.md)

## First boot

When ready, every monitors connected to device should display an fullscreen Scenic wallpaper.

Move your mouse cursor to bottom of screen, taskbar panel will appear.

You can click on start menu at bottom left corner to access applications.

## Setup Scenic

Start Scenic setup wizard by launching `Scenic setup wizard` entry located in start menu, under `Scenic` section or with `scenic-setup` command.

Type `YES` to proceed with setup.

```bash
scenicbox@scenicbox:~$ scenic-setup 
Welcome to SCENIC setup

Setup will download and start SCENIC.

Do you want to start setup? Type YES to proceed: YES
```

Type `YES` to install custom Scenic Station configuration. Menus and Bundles files will be downloaded and installed.

```
Custom menus and bundles configuration is needed to run Scenic Station hardware.
Press enter to skip custom configuration, or type YES to apply it: YES

Downloading Scenic Station config script...
```

Type `YES` to configure NVIDIA multiple GPU cards support (Base Mosaic). Enter sudo password.

```

Custom X server configuration is needed to support multiple NVIDIA GPU.
Press enter to skip NVIDIA Base Mosaic support configuration, or type YES to apply it: YES

Backing up existing xorg.conf file if present...
[sudo] password for scenicbox: 
mv: cannot stat '/etc/X11/xorg.conf': No such file or directory

Enabling NVIDIA Base Mosaic support...

WARNING: Unable to locate/open X configuration file.

Option "BaseMosaic" "True" added to Screen "Screen0".
New X configuration file written to '/etc/X11/xorg.conf'


Please restart computer to finish enable NVIDIA Base Mosaic support...
```

If you need multiple NVIDIA GPU support, you'll have to restart computer before being able to configure monitors connected to other cards.

Enter URL to download `contacts.json` file.

```
If you were provided a custom download URL for contact list, enter it here.
Press enter to skip contact download, or enter custom URL to download it: http://.../contacts.json

Downloading contact list file script...
```

Press enter to deploy latest version of Scenic Web application.

```
Press enter to download latest release image of Scenic, or enter new image tag to override.
scenic tag to deploy [master]:
```

Press enter to deploy latest version of Scenic backend.

```
Press enter to download latest release image of Scenic-core, or enter new image tag to override.
scenic-core tag to deploy [master]:
```

Scenic will be downloaded and started. Setup will display application status then wait for input to exit.

Press enter to exit setup wizard.

```
Detected NVIDIA GPU, using nvidia profile.

Stopping SCENIC...

Downloading Docker Compose file...

Downloading SCENIC...
[+] Running 14/14
 ⠿ scenic-core-nvidia Pulled                                                                                           28.0s
   ⠿ 846c0b181fff Pull complete                                                                                         1.1s
   ⠿ 4b32e7f24a7d Pull complete                                                                                         1.2s
   ⠿ 604636cfb89f Pull complete                                                                                        25.5s
   ⠿ 4f4fb700ef54 Pull complete                                                                                        25.6s
   ⠿ 588fef56f046 Pull complete                                                                                        27.1s
   ⠿ 1a5be3a21f7d Pull complete                                                                                        27.2s
   ⠿ 32d3f6f2ed70 Pull complete                                                                                        27.3s
   ⠿ a0ab0dcfd4f0 Pull complete                                                                                        27.4s
 ⠿ scenic-web Pulled                                                                                                    3.9s
   ⠿ 8921db27df28 Already exists                                                                                        0.0s
   ⠿ d793bae7cc0b Pull complete                                                                                         1.5s
   ⠿ 02b7e82b25c9 Pull complete                                                                                         2.8s
   ⠿ cd0ee4cafa6d Pull complete                                                                                         3.2s

Starting SCENIC...
[+] Running 3/3
 ⠿ Network scenicbox_default  Created                                                                                   0.0s
 ⠿ Container scenic           Started                                                                                   0.5s
 ⠿ Container scenic-core      Started                                                                                   0.4s


Configuration done.

Here is SCENIC setup:

CONTAINER ID   IMAGE                                                         COMMAND                  CREATED                  STATUS                  PORTS                                       NAMES
76553b42194c   registry.gitlab.com/sat-mtl/tools/scenic/scenic-core:master   "/docker-entrypoint.…"   Less than a second ago   Up Less than a second                                               scenic-core
cfb1c9ff3e9a   registry.gitlab.com/sat-mtl/tools/scenic/scenic:master        "/bin/sh -c 'h2o -m …"   Less than a second ago   Up Less than a second   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   scenic

Setup is completed.
Press any key to quit.
```

## Setup JACK

Start JACK audio interface selection wizard by launching `Setup JACK audio interface` entry located in start menu, under `ScenicOS` section, or with `scenicos-setup-jack` command.

Enter number `3` to select Behringer X AIR XR18 audio interface.

```bash
scenicbox@scenicbox:~$ scenicos-setup-jack 
Welcome to JACK setup

Setup will display a list of audio interfaces available.

You will be able to choose which audio interface to use for JACK.

Available sound interfaces list:
Number  Name
0:       dummy
1:       NVidia
2:       PCH
3:       X18XR18

Select number of which sound interface to use: 3

Selected "X18XR18" sound interface.
```

Setup will start JACK service and display its status then wait for input to exit.

Press enter to exit wizard.

```
Configuration done.

Jack server status:

● jack@scenicbox.service - JACK server using scenicbox.conf profile
     Loaded: loaded (/home/scenicbox/.config/systemd/user/jack@.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2023-02-07 08:22:45 EST; 1s ago
       Docs: man:jackd(1)
   Main PID: 838005 (jackd)
     CGroup: /user.slice/user-1000.slice/user@1000.service/jack.slice/jack@scenicbox.service
             └─838005 /usr/bin/jackd --realtime -d alsa -d hw:X18XR18,0 --rate 48000 --period 1024 --nperiods 3 --duplex

Feb 07 08:22:45 scenicbox jackd[838005]: JACK server starting in realtime mode with priority 10
Feb 07 08:22:45 scenicbox jackd[838005]: self-connect-mode is "Don't restrict self connect requests"
Feb 07 08:22:45 scenicbox jackd[838005]: audio_reservation_init
Feb 07 08:22:45 scenicbox jackd[838005]: Acquire audio card Audio1
Feb 07 08:22:45 scenicbox jackd[838005]: creating alsa driver ... hw:X18XR18,0|hw:X18XR18,0|1024|3|48000|0|0|nomon|s…|-|32bit
Feb 07 08:22:45 scenicbox jackd[838005]: configuring for 48000Hz, period = 1024 frames (21.3 ms), buffer = 3 periods
Feb 07 08:22:45 scenicbox jackd[838005]: ALSA: final selected sample format for capture: 32bit integer little-endian
Feb 07 08:22:45 scenicbox jackd[838005]: ALSA: use 3 periods for capture
Feb 07 08:22:45 scenicbox jackd[838005]: ALSA: final selected sample format for playback: 32bit integer little-endian
Feb 07 08:22:45 scenicbox jackd[838005]: ALSA: use 3 periods for playback
Hint: Some lines were ellipsized, use -l to show in full.

Setup is completed.
Press any key to quit.
```

## Open Scenic

Click on `Scenic Web app` icon in taskbar panel.

## Connect to Scenic remotely

Right-click on network configuration icon at bottom right corner of desktop. Select `Connection Information` option in context-menu.

Take note of `IPv4` > `IP Address`.

Enter URL in your remote computer browser, replacing<scenic-IP> with Scenic IP you noted: `http://<scenic-IP>:8080/?endpoint=<scenic-IP>:8000`

# Install and configure TeamViewer

Start TeamViewer setup wizard by launching `Install and setup TeamViewer` entry located in start menu, under `ScenicOS` section, or with `scenicos-setup-teamviewer` command.

Type `YES` to proceed with setup.

```bash
scenicbox@scenicbox:~$ scenicos-setup-teamviewer 
Welcome to TeamViewer setup

Setup will download, install latest TeamViewer and configure a random password.

You will need to enter sudo password to allow installation and configuration.

Do you want to start setup? Type YES to proceed: YES

```

Type `YES` to setup TeamViewer.

You will be asked for sudo password as we need permission to install application.

```
Downloading TeamViewer package...

Installing TeamViewer...
[sudo] password for scenicbox:
```

Press enter key to accept TeamViewer license agreement.

```
Press any key to accept TeamViewer license agreement: 
```

Before pressing enter to restart, **take note of your TeamViewer ID and password** and send it to Scenic support team.

```
Configuration done.

Here is your TeamViewer connection setup:

	TeamViewer version:  15.39.3
	TeamViewer ID:       1582519743
	TeamViewer password: KLWWj5RwmNS7wS6J

Take note of information before exiting.
```

Press enter key to restart desktop environment.

```
Setup is completed. Display manager and TeamViewer need to be restarted to allow capture desktop.
Press any key to restart display manager:
```

# Remotely manage Scenic station

Scenic support team can use Mender to remotely manage Scenic station. This steps shows how to accept and connect to newly installed Scenic station.

If connected to Internet, mender-client agent service running on device should already have sent a new authorization request to Mender server.

Next step is to accept and test remote management from Mender application.

## Accept new device authorization request

 1. Logon to https://hosted.mender.io
 2. Click on Pending devices.
 3. Select new device from list.
 4. Click on Accept link.

## Open remote terminal

 1. Open Devices panel.
 2. Select new device.
 3. Select Troubleshooting tab, and click on Launch a new Remote Terminal session.
 4. Login to `scenicbox` user with `su - scenicbox` command.

```bash
nobody@scenicbox:/$ su - scenicbox
Password: <enter password>
scenicbox@scenicbox:~$
```