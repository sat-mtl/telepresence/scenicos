# Build ScenicOS image

## Download ScenicOS repository

```bash
sudo apt install git git-lfs
git clone https://gitlab.com/sat-mtl/distribution/scenicos.git
cd scenicos
git submodule update --init
```

Apply ScenicOS customizations to mender-convert.

```bash
patch -p0 < 3rdparty/mender-convert.patch
```

## Dependencies intallation

Install necessary tools using apt for base image creation.

```bash
sudo apt install gdisk dosfstools debootstrap zerofree
```

Install necessary tools using apt for mender-convert.

```bash
sudo apt install $(cat 3rdparty/mender-convert/requirements-deb.txt)
```

Install mender-artifact.

```bash
MENDER_ARTIFACT_VERSION=3.7.x
sudo wget -q -O /usr/bin/mender-artifact https://downloads.mender.io/mender-artifact/$MENDER_ARTIFACT_VERSION/linux/mender-artifact
sudo chmod +x /usr/bin/mender-artifact
```

## Usage

Run `build-base-image script` to generate a base image using debootstrap. Pass board name as argument. You need to use sudo to run as root for loopback image mount to work.

```bash
sudo scripts/build-base-image.sh x86-64
```

The resulting image is stored in build/x86-64/base-image/ and can be used as-is if you don't need Mender integration.

You can test base image by starting it with QEMU.

```bash
scripts/start-image-qemu.sh build/x86-64/base-image/*.img
```

Run `convert-base-image-to-mender` to convert base image to Mender format using mender-convert. Pass board name as argument. You need to use sudo to run as root for loopback image mount to work.

```bash
sudo scripts/convert-base-image-to-mender.sh x86-64
```

Final image file located in build/x86-64/deploy/ is ready to be flashed to an SD card, USB flash drive or a hard drive using Balena Etcher.

`.mender` artifact file can be uploaded to Mender server as a new release and be deployed to device remotely. This file contains only new rootfs to do an OS update. It does not contain data partition.

## Test image using QEMU

If you want to test data partition auto-grow feature, you can resize image file to bigger size before starting it.

```bash
truncate -s 64G build/x86-64/deploy/oscos-bullseye-amd64-x86-64-minimal-mender-*.img
```

Start virtual machine using newly created image.

```bash
scripts/start-image-qemu.sh build/x86-64/deploy/scenicos-focal-amd64-x86-64-mender-*.img
```

Connect to image running via QEMU portfoward.

```bash
ssh -p 2222 scenicbox@localhost
```

## Upload artifact to Mender server from CLI

Install mender-cli tool.

```bash
wget https://downloads.mender.io/mender-cli/1.7.0/linux/mender-cli
sudo chmod +x mender-cli
sudo cp mender-cli /usr/local/bin/
```

Login to Mender server.

```bash
mender-cli login
```

Upload artifact to Mender server.

```bash
mender-cli artifacts upload build/x86-64/deploy/*.mender
```

## Rebuild scenicos-image-builder image

We use `registry.gitlab.com/sat-mtl/telepresence/scenicos/scenicos-image-builder` image to debootstrap base image, convert it to Mender format and upload it to Mender server and AWS S3 bucket.

Here's the steps required if you need to update scenicos-image-builder image.

```bash
cd docker/scenicos-image-builder
docker build \
    -t registry.gitlab.com/sat-mtl/distribution/scenicos/scenicos-image-builder:latest \
    .
docker login registry.gitlab.com
docker push registry.gitlab.com/sat-mtl/distribution/scenicos/scenicos-image-builder:latest
```

## CI image builder process

This pipeline uses Docker runner in a particular way because it needs to access Runner's host kernel loopback module to mount image files.

Jobs start sibling containers to pass thru `/dev` from host to allow to create and access `/dev/loop*` devices. This is less robust and is prone to leave stray resources over from canceling jobs.

Manual-build-base-image job starts one sibling container that creates a bootable base image using debootstrap. Base image is compressed as gzip and uploaded on a temporary storage S3 bucket `scenicos-ci-temporary` under a subfolder unique to job's branch.
Manual-convert-image-to-mender job starts `scenicos-${CI_COMMIT_REF_SLUG}-mender-convert-prepopulate` container to download base image from S3 bucket into data volume `scenicos-${CI_COMMIT_REF_SLUG}-project-data`.

Data volume is then attached to second `scenicos-${CI_COMMIT_REF_SLUG}-mender-convert-process` container which calls `convert-base-image-to-mender.sh` script. This script transfers base image to second data volume `scenicos-${CI_COMMIT_REF_SLUG}-mender-convert-data`. This volume is finally attached to a third container started from mender-convert image.

Artifact and final bootable image are compressed using lzma (.xz format). This is the most time consuming step of the process. Gzip compression can be configure using variable `MENDER_ARTIFACT_COMPRESSION` in boards/x86-64/mender_image_config file. This allows for quicker turn-around but results in final files double in size.

After conversion, Mender artifact file gets uploaded to Mender server so it is available for Over-the-Air updates.
Final bootable image including Mender client agent is uploaded to `scenicos-release` S3 bucket. Download URL is displayed in pipeline job log.